import {useEffect, useState} from 'react'
import Keycloak from 'keycloak-js';

const useKeyCloak = () => {

  const [keycloak, setKeycloak] = useState({})
  const [isAuthenticated, setIsAuthenticated] = useState(false)

  const _kc = new Keycloak('/keycloak.json');
  const handleKeyCloakInit = () =>{
   
    _kc.init(({ onLoad: 'check-sso' })).then(authenticated => {
      setKeycloak(_kc)
      setIsAuthenticated(authenticated)
      localStorage.setItem('kc_token', _kc.token);
      localStorage.setItem('kc_refreshToken', _kc.refreshToken);

    })
   console.log(_kc.hasRealmRole('app-customer')) 
  }

  useEffect(() => {
    handleKeyCloakInit()
  },[])


return [keycloak, isAuthenticated]
}

export default useKeyCloak

