import React from "react";
import { Card, CardBody, CardTitle, CardFooter } from "reactstrap";
import { Link } from "react-router-dom";

const DashBoardCards = ({ cardTitle, footerText, cardRoute }) => {
  return (
    <Card>
      <CardBody>
        <CardTitle tag="h4">{cardTitle}</CardTitle>
      </CardBody>
      <Link to={`/${cardRoute}`}>
        <CardFooter>
          <span className="d-inline-block">{footerText}</span>
          <span className="d-inline-block  float-right">
            <span className="material-icons">arrow_forward</span>
          </span>
        </CardFooter>
      </Link>
    </Card>
  );
};

export default DashBoardCards;
