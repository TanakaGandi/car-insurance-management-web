import React from 'react';
import {Card, CardHeader, CardBody} from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator';

import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

const AvailableTasksTable =({tasks})=>{
    const table = tasks;
  

    const columns = [
      {
        dataField: "id",
        text: "Id",
      },
      {
        dataField: "name",
        text: "Name",
      },
     
      {
        dataField: "assignee",
        text: "Assignee",
      },
    ];
    
    return (
      <div className="animated">
        <Card>
          <CardHeader>
            Available Tasks
          </CardHeader>
          <CardBody >
            <BootstrapTable  keyField='id' columns={ columns }  data={table} version="4" striped hover  pagination={ paginationFactory() } />
          </CardBody>
        </Card>
      </div>
    );

}

export default AvailableTasksTable;
