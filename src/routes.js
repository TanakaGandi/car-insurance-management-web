import React from "react";
import DefaultLayout from './containers/DefaultLayout';
const Dashboard = React.lazy(() => import("./screens/dashboard/Dashboard.js"));

//Claims
const Claims = React.lazy(() => import("./screens/claims/ClaimsScreen.js"));
const LodgeClaim = React.lazy(() => import("./screens/claims/LodgeClaim.js"));
const ClaimTypeScreen = React.lazy(() => import("./screens/claims/claimtypescreens/ClaimTypeScreen.js"))
const CarInsuranceClaimScreen = React.lazy(() => import("./screens/claims/claimtypescreens/CarInsuranceClaimScreen.js"))
const FuneralInsuranceClaimScreen = React.lazy(() => import("./screens/claims/claimtypescreens/FuneralInsuranceClaimScreen.js"))
const MedicalInsuranceClaimScreen = React.lazy(() => import("./screens/claims/claimtypescreens/MedicalInsuranceClaimScreen.js"))

const CancelClaim = React.lazy(() => import("./screens/claims/CancelClaim.js"));
const DealersScreen = React.lazy(() => import("./screens/dealers/DealersScreen.js"));
const SubmitDocs = React.lazy(() => import("./screens/claims/SubmitDocumentation.js"));
const TrackClaim = React.lazy(() => import("./screens/claims/trackclaimscreens/TrackClaim.js"));
const AdministerClaim = React.lazy(() => import("./screens/claims/admin/AdministerClaim.js"));
const DamageScreen = React.lazy(() => import("./screens/claims/damageassesmentscreens/DamageScreen.js"));
const QuotationsScreen = React.lazy(() => import("./screens/quotations/QuotationsScreen.js"))
const CompanysScreen = React.lazy(() => import("./screens/quotations/CompanysScreen.js"))
const AvailableTasks = React.lazy(() => import("./screens/claims/admin/AvailableTasks.js"))
const ApproveTasks = React.lazy(() => (import("./screens/claims/admin/ApproveTasks.js")))
const ViewEvidence = React.lazy(() => (import("./screens/claims/ViewEvidence.js")))
const NewApplication = React.lazy(() => (import("./screens/applications/NewApplication.js")))
const MoreDetails = React.lazy(() => import("./screens/details/MoreDetails.js"))
const HandleAssessment = React.lazy(() => import("./screens/details/AssignConsultant.js"))
const InspectVehicle = React.lazy(() => import("./screens/details/InspectVehicle.js"))
const InsuranceTypeScreen = React.lazy(() => import("./screens/applications/InsuranceTypeScreen.js"))
const ApplicantDetailsScreen = React.lazy(() => import("./screens/applications/ApplicantDetailsScreen.js"))
const UnderwriterTaskScreen = React.lazy(() => import("./screens/applications/underwriterTasks/UnderwriterTaskScreen.js"))
const UnderwritingDetailsScreen = React.lazy(() => import("./screens/applications/underwriterTasks/UnderwritingDetailsScreen.js"))
const HealthInsuranceDetailsScreen = React.lazy(() => import("./screens/applications/underwriterTasks/HealthInsuranceDetailsScreen.js"))


const routes = [
  //Breadcrumbs
  { path: '/', name: 'Home', component: DefaultLayout, exact: true },
  //Dashboard
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  //Claims
  { path: "/claims", name: "Claim Management", component: Claims, exact: true },
  { path: "/claims/lodge", name: "Lodge Claim", component: LodgeClaim, exact: true },
  { path: "/claims/type", name: "Select Claim Type", component: ClaimTypeScreen, exact: true },
  { path: "/claims/type/car", name: "Car Insurance Claim", component: CarInsuranceClaimScreen, exact: true },
  { path: "/claims/type/funeral", name: "Funeral Insurance  Claim", component: FuneralInsuranceClaimScreen, exact: true },
  { path: "/claims/type/medical", name: "Medical Insurance  Claim", component: MedicalInsuranceClaimScreen, exact: true },
  { path: "/cancel", name: "Cancel Claim", component: CancelClaim },
  { path: "/submit", name: "Submit Documentation", component: SubmitDocs },
  { path: "/track", name: "Track Claim", component: TrackClaim },
  { path: "/adminster", name: "Adminster Claims", component: AdministerClaim },
  { path: "/damage", name: "Assess Damage", component: DamageScreen },
  { path: "/quotation", name: "Quotations", component: QuotationsScreen },
  { path: "/quotes", name: "Quotations", component: CompanysScreen },
  { path: "/available-tasks", name: "Available Tasks", component: AvailableTasks },
  { path: "/approve-tasks", name: "Approve Tasks", component: ApproveTasks },
  //view-evidence/:id
  { path: "/view-evidence/:id", name: "Evidence", component: ViewEvidence },
  //Dealers
  { path: "/find", name: "Service Providers", component: DealersScreen },
  //More Details
  { path: "/more-details/:processInstanceId", name: "More Details", component: MoreDetails },
  //Handle Accessment
  { path: "/handle-assessment/:id", name: "Assessment", component: HandleAssessment },
  //Inspect Vehicle
  { path: "/inspect-vehicle/:id", name: "Inspect Vehicle", component: InspectVehicle },
  //New Application Business
  { path: "/new", name: "New Business Application", component: NewApplication, exact: true },
  { path: "/new/type", name: "Insurance Type", component: InsuranceTypeScreen, exact: true },
  { path: "/new/type/details", name: "Applicant Details", component: ApplicantDetailsScreen, exact: true },
  { path: "/task/underwriter", name: "Underwriter Tasks", component: UnderwriterTaskScreen, exact: true },
  { path: "/task/underwriter/details", name: "Client Details", component: UnderwritingDetailsScreen, exact: true },
  { path: "/task/underwriter/complete", name: "Complete Medical Insurance Application", component: HealthInsuranceDetailsScreen, exact: true }

];

export default routes;
