export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
    },
    {
      name: "Claims Management",
      icon: "icon-star",
      children: [
        {
          name: 'Claims',
          url: '/claims',
          icon: 'icon-puzzle'
        },
        {
          name: 'Lodge Claim',
          url: '/lodge',
          icon: 'icon-puzzle',
        },
        {
          name: 'Track Claim',
          url: '/track',
          icon: 'icon-puzzle',
        },
        {
          name: 'Cancel Claim',
          url: '/cancel',
          icon: 'icon-puzzle',
        },
        {
          name: 'Find A Dealer',
          url: '/find',
          icon: 'icon-puzzle',
        },
        {
          name: 'Submit Documentation',
          url: '/submit',
          icon: 'icon-puzzle',
        },
      ]
    },
  ],
};
