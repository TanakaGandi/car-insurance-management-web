import "react-app-polyfill/ie9"; // For IE 9-11 support
import "react-app-polyfill/ie11"; // For IE 11 support
import axios from 'axios'
import "./polyfill";
import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "./i18next";
import '../node_modules/font-awesome/css/font-awesome.min.css'; 
import * as serviceWorker from "./serviceWorker";


//Axios Request Interceptors => Before Request
axios.interceptors.request.use((request)=>{
    console.log('**Request**',request)
    request.headers.Authorization = `Bearer ${localStorage.getItem('kc_token')}`;
    return request;
})

// const token = localStorage.getItem('kc_token')
// console.log(token)

// //Axios Response Interceptors => After Request
// axios.interceptors.response.use((response)=>{
//     console.log("**Response**", response)
//     return response;
// })


ReactDOM.render(
    <Suspense fallback="<div>loading~~~</div>">
        <App />
    </Suspense>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
