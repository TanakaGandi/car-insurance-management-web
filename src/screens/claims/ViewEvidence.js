import React, {useState, useEffect} from "react";
import './styles.css'
import axios from "axios"
import AllPages from '../../components/documents/AllPages'

const ViewEvidence = (props) => {
  const [info, setInfo] = useState([]);
  const [urlVa, setUrlVa] = useState();
  const {id} = props.match.params
  // let urlNew  = null
  useEffect(() => {
    if (id) {
      retreiveDocumentByProcessId(id)
    }
   
    
  }, [id])


  const retreiveDocumentByProcessId = async(id) =>{
    const {data} = await axios.get(`http://localhost:9002/api/v1/files/process/${id}`)
    console.log("**Info Data**", data[0].downloadUrl )
    setUrlVa(data)
    setInfo(data)
    console.log("Url Va", urlVa)
    console.log("Info Value", info)

    
    console.log("**Id Value From Params**", id)


    
  }

  return (
    <div className="all-page-container">
     {


    info.map(dwnUrl => 
      <AllPages key={dwnUrl.id} pdf={dwnUrl.downloadUrl} /> 
      )
     }

      
    </div>
  );
};

export default ViewEvidence;
