import React from "react";
import {Link} from 'react-router-dom'
import { useForm } from "react-hook-form";
import {
  Container,
  Col,
  Button,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
} from "reactstrap";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object().shape({
  clientId: yup
    .string()
    .required("Required!")
    .min(6, "Too Short")
    .max(15, "Too Long"),
  clientFirstName: yup.string().required("Required!"),
  clientSurname: yup.string().required("Required!"),
  carType: yup.string().required("Required!"),
  carRegNumber: yup.string().required("Required!"),
  clientEmail: yup
    .string()
    .email("Please Enter a Valid Email!")
    .required("Required"),
  description: yup.string().required("Pleae Describe Accident"),
});

const LodgeClaim = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {

    const formData = new FormData();
    formData.append("clientId", `${data.clientId}`);
    formData.append("clientFirstName", `${data.clientFirstName}`);
    formData.append("clientSurname", `${data.clientSurname}`);
    formData.append("carType", `${data.carType}`);
    formData.append("carRegNumber", `${data.carRegNumber}`);
    formData.append("clientEmail", `${data.clientEmail}`);
    formData.append("description", `${data.description}`);

    const response = await axios.post(`/api/v1/request-claim`, formData);
    console.log(data)
    console.log("**Post Response Data**", response.data);
  };

  return (
    <>
    <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Card>
        <form onSubmit={handleSubmit(onSubmit)}>
          <CardHeader>
            <strong>Create Claim</strong>
          </CardHeader>

          <CardBody>
            <FormGroup  row className="my-0">
              
              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="clientID">Insured Client ID.</Label>
                  <Input
                    type="text"
                    {...register("clientId")}
                    className="form-control mb-3"
                    placeholder="Client Id"/>
                  {errors.clientId && (
                    <p className="text-danger">{errors.clientId.message}</p>
                  )}
                </FormGroup>
              </Col>

              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="clientEmail">Client Email</Label>
                  <Input
                    type="text"
                    {...register("clientEmail")}
                    className="form-control mb-3"
                    placeholder="Client Email"/>
                  {errors.clientEmail && (
                    <p className="text-danger">{errors.clientEmail.message}</p>
                  )}
                </FormGroup>
              </Col>

            </FormGroup>

            <FormGroup row className="my-0 ">

            <Col xs="6">
            <FormGroup>
              <Label htmlFor="clientFirstName">Client Firstname</Label>
              <Input
                type="text"
                {...register("clientFirstName")}
                className="form-control mb-3"
                placeholder="Client Firstname"
              />
              {errors.clientFirstName && (
                <p className="text-danger">{errors.clientFirstName.message}</p>
              )}
            </FormGroup>
            </Col>
            <Col xs="6">
            <FormGroup>
              <Label htmlFor="clientSurname">Client Lastname</Label>
              <Input
                type="text"
                {...register("clientSurname")}
                className="form-control mb-3"
                placeholder="Client Lastname"
              />
              {errors.clientSurname && (
                <p className="text-danger">{errors.clientSurname.message}</p>
              )}
            </FormGroup>
            </Col>
            </FormGroup>

          <FormGroup row className="my-0 ">
            <Col xs="6">
            <FormGroup>
              <Label htmlFor="carType">Car Type</Label>
              <select {...register("carType")} className="form-control mb-3">
                <option value="null">Select Car Type</option>
                <option value="Jeep">Jeep</option>
                <option value="Toyota">Toyota</option>
                <option value="BMW">BMW</option>
              </select>
              {errors.carType && (
                <p className="text-danger">{errors.carType.message}</p>
              )}
            </FormGroup>
            </Col>
            <Col xs="6">
            <FormGroup>
              <label htmlFor="carRegNumber">Insured Reg. Number</label>
              <Input
                type="text"
                {...register("carRegNumber")}
                className="form-control mb-3"
                placeholder="Car Registration Number"
              />
              {errors.carRegNumber && (
                <p className="text-danger">{errors.carRegNumber.message}</p>
              )}
            </FormGroup>
            </Col>

            </FormGroup>


            <FormGroup>
              <Label htmlFor="description">Accident Description</Label>
              <textarea
                {...register("description")}
                className="form-control mb-3"
                placeholder="Description"
              />
              {errors.description && (
                <p className="text-danger">{errors.description.message}</p>
              )}
            </FormGroup>
          </CardBody>
          <CardFooter>
            <Button className="btn btn-success">Request Claim</Button>
          </CardFooter>
        </form>
      </Card>
    </>
  );
};

export default LodgeClaim;