import React from "react";
import { Row, Col } from "reactstrap";
import { Link } from "react-router-dom" 
import useTrans from "../../../hooks/useTrans";
import ScreensCards from "../../../components/cards/ScreensCards";

const AdministerClaim = () => {
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();
  return (
    <div className="animated fadeIn">
      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>

      <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("AvailableClaims.1")}
            footerText={t("Available.1")}
            cardRoute="available-tasks"
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("SearchClaims.1")}
            footerText={t("Search.1")}
            cardRoute="adminster"
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("ApproveClaims.1")}
            footerText={t("Approve.1")}
            cardRoute="approve-tasks"
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("RejectClaims.1")}
            footerText={t("Reject.1")}
            cardRoute="adminster"
          />
        </Col>
      </Row>
    </div>
  );
};

export default AdministerClaim;
