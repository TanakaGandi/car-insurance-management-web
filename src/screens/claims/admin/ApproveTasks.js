import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import { Container } from "reactstrap"
import ApproveTasksTable from "../../../components/tables/ApproveTasksTable"


const ApproveTasks = () => {

    const[task, setTask] = useState([]);

    useEffect(() => {

      fetchAvailableTasks()
      
    },[])
  
  
    const fetchAvailableTasks = async()=>{
      const response = await axios.get(`/api/v1/tasks/my-tasks`)
      setTask(response.data)
      console.log("Data In Approve", response.data)
    }

    //Handle Approve Task
      const handleApproveTask = async(event) => {
        const res  = await axios.post(`/api/v1/task/${event.id}/complete`,{
          approved: true,
          autoAssess: true
        })
        fetchAvailableTasks()
        console.log('**Response Data ==> Task Completed**', res.data)
      }


      //Get More Details Task
      const handleMoreDetailsTask = async(event) =>{
        const res = await axios.post(`/api/v1/task/${event.id}/complete`,{
          getMoreDetails: true
        })
        fetchAvailableTasks()
        console.log('**Response Data ==> Get More Details Task**', res.data)
      }

      //Handle Reject Task
      const handleRejectedTask= async(event) =>{
        const res  = await axios.post(`/api/v1/task/${event.id}/complete` ,{
          approved: false
        })
        fetchAvailableTasks()
        console.log('**Respose Data ==> Task Rejected**', res.data)
      }


    return (
        <>
          <Link to="/adminster" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
          <ApproveTasksTable 
              tasks ={task} 
              handleApproveTask = {handleApproveTask}
              handleRejectedTask = {handleRejectedTask}
              handleMoreDetails = {handleMoreDetailsTask}
          />
      </>
    )
}

export default ApproveTasks
