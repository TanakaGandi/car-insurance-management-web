import React from "react";
import {Row, Col} from 'reactstrap'
import useTrans from "../../hooks/useTrans";
import { Link } from "react-router-dom"
import ScreensCards from "../../components/cards/ScreensCards";

const ClaimsScreen = () => {

  // eslint-disable-next-line
  const [t, handleClick] = useTrans();
  
  return (
    <div className="animated fadeIn">

      <Link to="/dashboard" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("LodgeClaim.1")}
            footerText={t("Claim.1")}
            cardRoute="claims/type"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("DamageAssessment.1")}
            footerText={t("Damage.1")}
            cardRoute="damage"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("TrackClaim.1")}
            footerText={t("Track.1")}
            cardRoute="track"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("CancelClaim.1")}
            footerText={t("Cancel.1")}
            cardRoute="cancel"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("SubmitDocumentation.1")}
            footerText={t("Submit.1")}
            cardRoute="submit"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("Administer.1")}
            footerText={t("Admin.1")}
            cardRoute="adminster"
          />
        </Col>
      </Row>
    </div>
  );
};

export default ClaimsScreen;
