import React from 'react';
import axios from "axios"
import * as Yup from 'yup'
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';
import { onSuccess, onError } from "../../../utils";

const validationSchema = function (values) {
  return Yup.object().shape({
    clientId: Yup.string()
    .required('Client ID is Required!'),
    clientFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is Required!'),
    clientSurname: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is Required'),
    clientEmail: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is Required!'),
    carType: Yup.string()
    .required('Car Type is required!'),
    carRegNumber: Yup.string()
    .required('Registration Number is Required!'),
    description: Yup.string()
    .required('Description is Required!'),
  
  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  clientId: "",
  clientFirstName: "",
  clientSurname: "",
  clientEmail: "",
  carType:"",
  carRegNumber:"",
  description:"",
  

  
}

const onSubmit =  (values, { setSubmitting, setErrors, resetForm}) => {

  const formData = new FormData();
  formData.append("clientId" , `${values.clientId}`)
  formData.append("clientFirstName" , `${values.clientFirstName}`)
  formData.append("clientSurname" ,`${values.clientSurname}`)
  formData.append("carType", `${values.carType}`)
  formData.append("carRegNumber",`${values.carRegNumber}`)
  formData.append("clientEmail",`${values.clientEmail}`)
  formData.append("description", `${values.description}`)


  setTimeout(() => {
    axios
      .post(`/api/v1/request-claim`, formData)
      .then((data) => {
        resetForm({})
        onSuccess()
        console.log(data.data);
      })
      .catch((error) => {
        console.log(error);
        onError()
      });
      setSubmitting(false)

  }, 2000)
 
}



const CarInsuranceClaimScreen = () =>{
 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        clientId: true,
        clientFirstName: true,
        clientSurname: true,
        clientEmail: true,
        carType: true,
        carRegNumber:true,
        description:true,
      }
    )
    validateForm(errors)
  }
  return(
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    <Card>
      <CardHeader>
       <strong>Lodge Claim</strong>
      </CardHeader>
      <CardBody>
        <strong>Claim Details</strong>
        <hr />
        <Formik
          initialValues={initialValues}
          validate={validate(validationSchema)}
          onSubmit={onSubmit}
          render={
            ({
              values,
              errors,
              touched,
              status,
              dirty,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              isValid,
              handleReset,
              setTouched
            }) => (
              <>
                <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                  <FormGroup  row className="my-0">
              
              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="clientId">Client ID</Label>
                  <Input type="text"
                             name="clientId"
                             id="clientId"
                             placeholder="Client ID"
                             autoComplete="clientId"
                             valid={!errors.clientId}
                             invalid={touched.clientId && !!errors.clientId}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientId || ''} />
                    <FormFeedback>{errors.clientId}</FormFeedback>
                  
                </FormGroup>
              </Col>

              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="clientEmail">Client Email</Label>

                  <Input type="email"
                             name="clientEmail"
                             id="clientEmail"
                             placeholder="Email"
                             autoComplete="email"
                             valid={!errors.clientEmail}
                             invalid={touched.clientEmail && !!errors.clientEmail}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientEmail || ''} />
                      <FormFeedback>{errors.clientEmail}</FormFeedback>
                  

                </FormGroup>
              </Col>

            </FormGroup>

            <FormGroup row className="my-0 ">

            <Col xs="6">
            <FormGroup>
              <Label htmlFor="clientFirstName">Client First Name</Label>
              <Input type="text"
                              name="clientFirstName"
                              id="clientFirstName"
                              placeholder="First Name"
                              autoComplete="first-name"
                              valid={!errors.clientFirstName}
                              invalid={touched.clientFirstName && !!errors.clientFirstName}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientFirstName || ''} />
                        <FormFeedback>{errors.clientFirstName}</FormFeedback>
              
            </FormGroup>
            </Col>
            <Col xs="6">
            <FormGroup>
              <Label htmlFor="clientSurname">Client Last Name</Label>
              <Input type="text"
                             name="clientSurname"
                             id="clientSurname"
                             placeholder="Last Name"
                             autoComplete="username"
                             valid={!errors.clientSurname}
                             invalid={touched.clientSurname && !!errors.clientSurname}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientSurname || ''} />
                      <FormFeedback>{errors.clientSurname}</FormFeedback>
             
            </FormGroup>
            </Col>
            </FormGroup>

          <FormGroup row className="my-0 ">
            <Col xs="6">
            <FormGroup>
              <Label htmlFor="carType">Car Type</Label>
              <Input type="select" name="carType" id="carType"
                       valid={!errors.carType}
                       invalid={touched.carType && !!errors.carType}
                       required
                       onChange={handleChange}
                       onBlur={handleBlur}
                       value={values.carType} >
                        <option value="">Select Car Type</option>
                        <option value="Jeep">Jeep</option>
                        <option value="Toyota">Toyota</option>
                        <option value="BMW">BMW</option>
                      </Input>
                      <FormFeedback>{errors.carType}</FormFeedback>
            </FormGroup>
            </Col>
            <Col xs="6">
            <FormGroup>
              <label htmlFor="carRegNumber">Registration Number</label>
              <Input type="text"
                             name="carRegNumber"
                             id="carRegNumber"
                             placeholder="Enter Registration Number"
                             autoComplete="carRegNumber"
                             valid={!errors.carRegNumber}
                             invalid={touched.carRegNumber && !!errors.carRegNumber}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.carRegNumber || ''} />
                    <FormFeedback>{errors.carRegNumber}</FormFeedback>
            
            </FormGroup>
            </Col>

            </FormGroup>


            <FormGroup  row className="my-0 ">
              <Label htmlFor="description">Accident Description</Label>
              <Col  xs="12" sm="6"  lg="12">
                    <FormGroup>
                      <Input type="textarea"
                             name="description"
                             id="description"
                             placeholder="Description"
                             autoComplete="description"
                             valid={!errors.description}
                             invalid={touched.description && !!errors.description}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.description || ''} />
                    <FormFeedback>{errors.description}</FormFeedback>

                    </FormGroup>
                    </Col>
             
            </FormGroup>


            <FormGroup>
                      <Button type="submit" color="success" className="mr-1" disabled={isSubmitting || !isValid}>{isSubmitting ? 'Submitting' : 'Create Claim'}</Button>
                    </FormGroup>
                  </Form>
              </>
            )} />
      </CardBody>
    </Card>
  </div>
  )
}






export default CarInsuranceClaimScreen;
