import React from "react";
import { Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import ScreensCards from "../../../components/cards/ScreensCards";

const ClaimTypeScreen = () => {
  return (
    <div>
      <Link to="/dashboard" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Car Insurance Claim"}
            footerText={"Start Claim"}
            cardRoute="claims/type/car"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Funeral Insurance Claim"}
            footerText={"Start Claim"}
            cardRoute="claims/type/funeral"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Medical Insurance Claim"}
            footerText={"Start Claim"}
            cardRoute="claims/type/medical"
          />
        </Col>
      </Row>
    </div>
  );
};

export default ClaimTypeScreen;
