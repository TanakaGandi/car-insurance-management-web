import React,{useState} from 'react'
import { Link }  from 'react-router-dom'
import axios from 'axios'
import { Button,Form, FormGroup, Label, Input , Col } from 'reactstrap'

const InspectVehicle = (props) => {
    const [moreDetails, setMoreDetails] = useState(false);
    const { id } = props.match.params;


 
       const handleInspectVehicle = async() => {
        const res  = await axios.post(`/api/v1/task/${id}/complete`,{
          getMoreDetails: `${moreDetails}`
        })
        console.log('**Response Data ==> Task Completed**', res.data)
      }

      const handleState = () =>{
        setMoreDetails(!moreDetails)
      }

    return (
        <div className="animated fadeIn">
        <Link to="/approve-tasks" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
        <h3>Inspect Vehicle</h3>
        <Form onSubmit={handleInspectVehicle}>
        <FormGroup>
        <Label check className="form-check-label" htmlFor="checkbox1">More Details</Label>
            <Input 
                className="form-check-input ml-5" 
                type="checkbox" 
                checked ={moreDetails}
                onChange = {handleState} />
        </FormGroup>

        <Button className='btn btn-success' onClick={handleInspectVehicle}>Continue</Button>
        </Form>
    </div>
    )
}

export default InspectVehicle
