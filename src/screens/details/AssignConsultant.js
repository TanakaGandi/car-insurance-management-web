import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'
import { Button,Form, FormGroup, Label, Input , Col } from 'reactstrap'

const AssignConsultant = (props) => {
    const [autoAccess, setAutoAccess] = useState(false);
    const { id } = props.match.params;


 
       const handleApproveTask = async() => {
        const res  = await axios.post(`/api/v1/task/${id}/complete`,{
          autoAssess: `${autoAccess}`
        })
        console.log('**Response Data ==> Task Completed**', res.data)
      }

      const handleState = () =>{
        setAutoAccess(!autoAccess)
      }


    return (
        <div className="animated fadeIn">
          <Link to="/approve-tasks" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
            <h3>Assign  Assessor</h3>
            <Form onSubmit={handleApproveTask}>
            <FormGroup>
            <Label check className="form-check-label" htmlFor="checkbox1">Auto Access</Label>
                <Input 
                    className="form-check-input ml-5" 
                    type="checkbox" 
                    id="checkbox1" 
                    name="checkbox1" 
                    checked={autoAccess}
                    onChange = {handleState} />
                    
            </FormGroup>
        
            <Button className='btn btn-success' onClick={handleApproveTask}>Continue</Button>
            </Form>
        </div>
    )
}

export default AssignConsultant
