import React from "react";
import { Col, Row } from "reactstrap";
import DashBoardCards from "../../components/cards/DashBoardCards";
import useTrans from "../../hooks/useTrans";

const Dashboard = () => {
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Claims.1")}
            footerText={t("ManageClaims.1")}
            cardRoute = "claims"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Premium.1")}
            footerText={t("ManagePremiums.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Quotation.1")}
            footerText={t("ManageQuotations.1")}
            cardRoute="quotation"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Rewards.1")}
            footerText={t("ManageRewards.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Underwriting.1")}
            footerText={t("ManageUnderwriting.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Service.1")}
            footerText={t("ManageService.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Commissions.1")}
            footerText={t("ManageCommission.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
        <DashBoardCards
          cardTitle={t("ServiceProviders.1")}
          footerText={t("Providers.1")}
          cardRoute="find"
        />
      </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Register.1")}
            footerText={t("Regis.1")}
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={"New Business Application"}
            footerText={t("Apply.1")}
            cardRoute="new"
          />
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;
