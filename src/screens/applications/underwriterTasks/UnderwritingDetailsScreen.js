import React, { useState, useEffect } from "react";
import './Details.css'
import {
  Col,
  Row,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Table} from "reactstrap";
import { Link } from "react-router-dom";


const UnderwritingDetailsScreen = () => {

  const [task, setTask] = useState({});



  return (
    <div className="animated fadeIn">
        <Link to="/task/underwriter" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Card>
        <CardHeader>
          <strong>Client Details</strong>
        </CardHeader>
        <CardBody>
          <Table>
          <Row className="font__size">
            <Col xs="2"><span >National Id.</span></Col>
            <Col x6="10"><span className="ml-3 mt-3">{task.clientNationalId}</span></Col>
          </Row>

          <Row className="font__size"> <Col xs="2"><span>Email Address</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientEmailAddress}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>First Name</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientFirstName}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Last Name</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientLastName}</span></Col>
          </Row>

          <Row className="font__size">
              <Col xs="2"><span>Date of Birth</span></Col>
              <Col xs="10"><span className="ml-3 mt-3">{task.clientDateOfBirth}</span></Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Phone Number</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientPhoneNumber}</span> </Col>
          </Row>

        <Row className="font__size">
            <Col xs="2"><span>Gender</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientGender}</span> </Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Ocupation</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientOccupation}</span> </Col>
          </Row>

          <Row className="font__size">
            <Col xs="2"><span>Client Residential Address</span></Col>
            <Col xs="10"><span className="ml-3 mt-3">{task.clientResidentialAddress}</span> </Col>
          </Row>  

          </Table>
          

        </CardBody>
        <CardFooter>
          <Button className="btn btn-success ml-3 mt-3">Get More Details</Button>
        </CardFooter>
      </Card>
    </div>
  );
};

export default  UnderwritingDetailsScreen;
