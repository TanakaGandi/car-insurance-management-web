import React, { useState } from 'react';
import ReactFlagsSelect from 'react-flags-select';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';
import StepWizard from "react-step-wizard";

import * as Yup from 'yup'


const validationSchema = function (values) {
  return Yup.object().shape({
    nationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    clientFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    clientLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    clientEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    clientPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    clientGender: Yup.string()
    .required('Gender is required!'),
    clientAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    clientRegion: Yup.string()
    .required('Region is Required!'),
    clientZip: Yup.string()
    .required('Zip is Required!')
    .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/),
    clientCountry: Yup.string()
    .required('Client Country is Required!'),
    vehicleRegistrationNumber: Yup.string()
    .required('Vehicle Registration Number is Required!'),
    clientLicenseNumber: Yup.string()
    .required('Client License Number is Required!'),
    vehicleMake: Yup.string()
    .required('Vehicle Make is Required!'),
    vehicleModel: Yup.string()
    .required('Vehicle Model is Required!'),
    vehicleYear: Yup.string()
    .required('Vehicle Year is Required!'),
    vehicleMileage: Yup.string()
    .required('Vehicle Mileage is Required!'),
    vehicleWorth: Yup.string()
    .required('Vehicle Worth is Required!'),
    vehicleUsage: Yup.string()
    .required('Vehicle Usage is Required!'),
    numberOfSeats: Yup.string()
    .required('This field is Required!'),
    alarmSystemYesOrNo: Yup.string()
    .required('This field is Required!'),
    nightParkingYesOrNo: Yup.string()
    .required('This field is Required!'),
    isVehicleUsedByOthers: Yup.string()
    .required('This field is Required!'),
    dependentNationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    dependentLicenseNumber: Yup.string()
    .required('License Number is Required!'),
    relationshipToBeneficiary: Yup.string()
    .required('Relationship to Beneficiary is Required!'),
    dependentFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    dependentLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    dependentEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dependentDateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    dependentPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    dependentGender: Yup.string()
    .required('Gender is required!'),
    carInsuranceType: Yup.string()
    .required('Type of Car Insurance is Required!'),
    thirdPartyLiability: Yup.string()
    .required('Third Party Liability is Required!'),
    allRisksYesOrNo: Yup.string()
    .required('This field is Required!'),
    replacementCarYesOrNo: Yup.string()
    .required('This field is Required!'),
    totalLossYesOrNo: Yup.string()
    .required('This field is Required!'),
    excessYesOrNo: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceFrom: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceTo: Yup.string()
    .required('This field is Required!'),
    clientWorkAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientWorkAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    dateWhenLicenseWasObtained: Yup.string()
    .required('Date Is Required'),
    maritalStatus: Yup.string()
    .required('This Field Is Required')

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  nationalId: "",
  clientFirstName: "",
  clientLastName: "",
  clientEmailAddress: "",
  dateOfBirth: "",
  clientPhoneNumber: "",
  clientGender:"",
  clientAddressLineOne:"",
  clientAddressLineTwo:"",
  clientRegion:"",
  clientZip:"",
  clientCountry:"",
  vehicleRegistrationNumber:"",
  clientLicenseNumber:"",
  vehicleMake:"",
  vehicleModel:"",
  vehicleYear:"",
  vehicleMileage:"",
  vehicleWorth:"",
  vehicleUsage:"",
  numberOfSeats:"",
  alarmSystemYesOrNo:"",
  nightParkingYesOrNo:"",
  isVehicleUsedByOthers:"",
  dependentLicenseNumber:"",
  dependentNationalId: "",
  relationshipToBeneficiary: "",
  dependentFirstName: "",
  dependentLastName: "",
  dependentEmailAddress: "",
  dependentDateOfBirth: "",
  dependentPhoneNumber: "",
  dependentGender:"",
  carInsuranceType:"",
  thirdPartyLiability:"",
  allRisksYesOrNo:"",
  replacementCarYesOrNo:"",
  totalLossYesOrNo:"",
  excessYesOrNo:"",
  periodOfInsuranceFrom:"",
  periodOfInsuranceTo:"",
  clientWorkAddressLineOne:"",
  clientWorkAddressLineTwo:"",
  dateWhenLicenseWasObtained:"",
  maritalStatus:"",
}

const onSubmit = (values, { setSubmitting, setErrors }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const ContactDetails = () =>{


    const [selected, setSelected] = useState('');

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        nationalId: true,
        clientFirstName: true,
        clientLastName: true,
        clientEmailAddress: true,
        dateOfBirth: true,
        clientPhoneNumber: true,
        clientGender: true,
        clientAddressLineOne:true,
        clientAddressLineTwo:true,
        clientRegion:true,
        clientZip:true,
        clientCountry:true,
        vehicleRegistrationNumber:true,
        clientLicenseNumber:true,
        vehicleMake:true,
        vehicleModel:true,
        vehicleYear:true,
        vehicleMileage:true,
        vehicleWorth:true,
        vehicleUsage:true,
        numberOfSeats:true,
        alarmSystemYesOrNo:true,
        nightParkingYesOrNo:true,
        isVehicleUsedByOthers:true,
        dependentLicenseNumber:true,
        dependentNationalId:true,
        relationshipToBeneficiary:true,
        dependentFirstName:true,
        dependentLastName:true,
        dependentEmailAddress:true,
        dependentDateOfBirth:true,
        dependentPhoneNumber:true,
        dependentGender:true,
        carInsuranceType:true,
        thirdPartyLiability:true,
        allRisksYesOrNo:true,
        replacementCarYesOrNo:true,
        totalLossYesOrNo:true,
        excessYesOrNo:true,
        periodOfInsuranceFrom:true,
        periodOfInsuranceTo:true,
        clientWorkAddressLineOne:true,
        clientWorkAddressLineTwo:true,
        dateWhenLicenseWasObtained: true,
        
      }
    )
    validateForm(errors)
  }
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Contact Information</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                status,
                dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                handleReset,
                setTouched
              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <strong>Residential Address</strong>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <strong>Work Address</strong>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                      <Label for="clientAddressLineOne">Address Line One</Label>
                        <Input type="textarea"
                              name="clientAddressLineOne"
                              id="clientAddressLineOne"
                              placeholder="Address Line 1"
                              autoComplete="clientAddressLineOne"
                              valid={!errors.clientAddressLineOne}
                              invalid={touched.clientAddressLineOne && !!errors.clientAddressLineOne}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientAddressLineOne} />
                      <FormFeedback>{errors.clientAddressLineOne}</FormFeedback>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientWorkAddressLineOne">Address Line One</Label>
                      <Input type="textarea"
                            name="clientWorkAddressLineOne"
                            id="clientWorkAddressLineOne"
                            placeholder="Address Line 1"
                            autoComplete="clientWorkAddressLineOne"
                            valid={!errors.clientWorkAddressLineOne}
                            invalid={touched.clientWorkAddressLineOne && !!errors.clientWorkAddressLineOne}
                            autoFocus={false}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.clientWorkAddressLineOne} />
                    <FormFeedback>{errors.clientWorkAddressLineOne}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">               
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="clientAddressLineTwo">Address Line Two</Label>
                        <Input type="textarea"
                              name="clientAddressLineTwo"
                              id="clientAddressLineTwo"
                              placeholder="Address Line 2"
                              autoComplete="clientAddressLineTwo"
                              valid={!errors.clientAddressLineTwo}
                              invalid={touched.clientAddressLineTwo && !!errors.clientAddressLineTwo}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientAddressLineTwo} />
                      <FormFeedback>{errors.clientAddressLineTwo}</FormFeedback>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="clientWorkAddressLineTwo">Address Line Two</Label>
                        <Input type="textarea"
                              name="clientWorkAddressLineTwo"
                              id="clientWorkAddressLineTwo"
                              placeholder="Address Line 2"
                              autoComplete="clientWorkAddressLineTwo"
                              valid={!errors.clientWorkAddressLineTwo}
                              invalid={touched.clientWorkAddressLineTwo && !!errors.clientWorkAddressLineTwo}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientWorkAddressLineTwo} />
                      <FormFeedback>{errors.clientWorkAddressLineTwo}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientZip">Zip Code</Label>
                      <Input type="text"
                            name="clientZip"
                            id="clientZip"
                            placeholder="Enter Zip Code"
                            autoComplete="clientZip"
                            valid={!errors.clientZip}
                            invalid={touched.clientZip && !!errors.clientZip}
                            autoFocus={false}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.clientZip} />
                    <FormFeedback>{errors.clientZip}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="country">Country</Label>
                    <ReactFlagsSelect
                      selected={selected}
                      onSelect={code => setSelected(code)}
                    />
                    <FormFeedback>{errors.clientZip}</FormFeedback>
                    </FormGroup>
                    </Col>

                    </FormGroup>

 
                    </Form>

                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default ContactDetails;
