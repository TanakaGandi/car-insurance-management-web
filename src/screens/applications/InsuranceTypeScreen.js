import React from "react";
import {Link} from 'react-router-dom'
import { useForm } from "react-hook-form";
import {Col, Button, FormGroup, Label, Card,CardHeader,CardBody,CardFooter} from "reactstrap";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import './style.css'
 
const schema = yup.object().shape({
 insuranceType: yup
   .string()
   .required("Required!")
   .min(6, "Too Short")
   .max(15, "Too Long"),
});
 
const InsuranceTypeScreen = () => {
 const {
   register,
   handleSubmit,
   formState: { errors },
 } = useForm({
   resolver: yupResolver(schema),
 });
 
 const onSubmit = async (data) => {
 
   const formData = new FormData();
 
   formData.append("insuranceType", `${data.insuranceType}`);
 
   const response = await axios.post(`/api/v1/request-claim`, formData);
 
   console.log(data)
   console.log("**Post Response Data**", response.data);
 };
 
 return (
   <>
   <Link to="/new" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
     <Card>
       <form onSubmit={handleSubmit(onSubmit)}>
         <CardHeader>
           <strong>Insurance Type</strong>
         </CardHeader>
 
         <CardBody>
           <FormGroup  row className="my-0">
             <Col xs="12">
             <FormGroup>
             <Label htmlFor="insuranceType">Select Insurance Type</Label>
             <select {...register("insuranceType")} className="form-control mb-3">
               <option value="null">Type</option>
               <option value="healthinsurance">Health Insurance</option>
               <option value="vehicleinsurance">Vehicle Insurance</option>
               <option value="funeralinsurance">Funeral Insurance</option>
               <option value="householdinsurance">Household Insurance</option>
             </select>
             {errors.insuranceType && (
               <p className="text-danger">{errors.insuranceType.message}</p>
             )}
           </FormGroup>
             </Col>
 
           </FormGroup>
         </CardBody>
         <CardFooter>
           <Link to ="/new/type/details" className="btn btn-success">Next</Link>
         </CardFooter>
       </form>
     </Card>
   </>
 );
};
 
export default InsuranceTypeScreen;

