import React, { useState } from 'react';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Field, Formik, FormikConfig, FormikValues } from 'formik';
import { StepForm, Step } from "react-formik-step";
//import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import StepWizard from "react-step-wizard";
import * as Yup from 'yup'
import ReactFlagsSelect from 'react-flags-select';
import Stepper from 'react-stepper-horizontal';
import PersonalDetails from './PersonalDetails';
import ContactDetails from './ContactDetails';
import VehicleDetails from './VehicleDetails';
import OtherUsersDetails from './OtherUsersDetails';
import InsuranceQuestions from './InsuranceQuestions';
import DocumentUpload from './DocumentUpload';

const ApplicantDetailsScreen = () =>{

  const [selected, setSelected] = useState('');

  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);

  //const [value] = React.useContext(FormContext);
  const [currentPage, setCurrentPage] = useState(1);

  const sections = [
    { title: 'Pesornal Details' },
    { title: 'Contact Details' },
    { title: 'Vehicle Details' },
    { title: 'Other Users Details' },
    { title: 'Insurance Questions' },
    { title: 'Document Upload' },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
    //console.log(value);
  };

  const next = () => setCurrentPage((prev) => prev + 1);
  const prev = () => setCurrentPage((prev) => prev - 1);


  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };



  //const [country, setCountry] = useState('')

  //const [region, setRegion] = useState('')


/*   const handleCountryChange = (country) => {
    setCountry(country);
  }
  
  const handleRegionChange = (region) => {
    setRegion(region);
  } */

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }

  return(
    <Form onSubmit={handleSubmit}>
    <Stepper
    steps={sections}
    activeStep={currentPage}
    activeColor="blue"
    defaultBarColor="blue"
    completeColor="green"
    completeBarColor="green"
  />
  {currentPage === 1 && (
    <div>
    <PersonalDetails/>
      <button onClick={next}>Next</button>
      </div>
  )}
  {currentPage === 2 && (
    <div>
      <ContactDetails/>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <button onClick={prev}>Back</button>
        <button onClick={next}>Next</button>
      </div>
    </div>
    
  )}

  {currentPage === 3 && (
      <div>
     <VehicleDetails/>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <button onClick={prev}>Back</button>
      <button onClick={next}>Next</button>
      </div>
      </div>
    
  )}

  {currentPage === 4 && (
    <div>
    <OtherUsersDetails/>
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <button onClick={prev}>Back</button>
        <button onClick={next}>Next</button>
    </div>
    </div>
  )}

  {currentPage === 5 && (
    <div>
      <InsuranceQuestions/>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <button onClick={prev}>Back</button>
        <button onClick={next}>Next</button>
      </div>
    </div>
    
  )}

  {currentPage === 6 && (
      <div>
     <DocumentUpload/>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <button onClick={prev}>Back</button>
        <button onClick={handleSubmit}>Submit</button>
      </div>
      </div>
    
  )}

    </Form>
  )
}


export default ApplicantDetailsScreen;


