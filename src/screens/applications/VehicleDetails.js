import React from 'react';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';
import StepWizard from "react-step-wizard";

import * as Yup from 'yup'


const validationSchema = function (values) {
  return Yup.object().shape({
    nationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    clientFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    clientLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    clientEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    clientPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    clientGender: Yup.string()
    .required('Gender is required!'),
    clientAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    clientRegion: Yup.string()
    .required('Region is Required!'),
    clientZip: Yup.string()
    .required('Zip is Required!'),
    clientCountry: Yup.string()
    .required('Client Country is Required!'),
    vehicleRegistrationNumber: Yup.string()
    .required('Vehicle Registration Number is Required!'),
    clientLicenseNumber: Yup.string()
    .required('Client License Number is Required!'),
    vehicleMake: Yup.string()
    .required('Vehicle Make is Required!'),
    vehicleModel: Yup.string()
    .required('Vehicle Model is Required!'),
    vehicleYear: Yup.string()
    .required('Vehicle Year is Required!'),
    vehicleMileage: Yup.string()
    .required('Vehicle Mileage is Required!'),
    vehicleWorth: Yup.string()
    .required('Vehicle Worth is Required!'),
    vehicleUsage: Yup.string()
    .required('Vehicle Usage is Required!'),
    numberOfSeats: Yup.string()
    .required('This field is Required!'),
    alarmSystemYesOrNo: Yup.string()
    .required('This field is Required!'),
    nightParkingYesOrNo: Yup.string()
    .required('This field is Required!'),
    isVehicleUsedByOthers: Yup.string()
    .required('This field is Required!'),
    dependentNationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    dependentLicenseNumber: Yup.string()
    .required('License Number is Required!'),
    relationshipToBeneficiary: Yup.string()
    .required('Relationship to Beneficiary is Required!'),
    dependentFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    dependentLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    dependentEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dependentDateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    dependentPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    dependentGender: Yup.string()
    .required('Gender is required!'),
    carInsuranceType: Yup.string()
    .required('Type of Car Insurance is Required!'),
    thirdPartyLiability: Yup.string()
    .required('Third Party Liability is Required!'),
    allRisksYesOrNo: Yup.string()
    .required('This field is Required!'),
    replacementCarYesOrNo: Yup.string()
    .required('This field is Required!'),
    totalLossYesOrNo: Yup.string()
    .required('This field is Required!'),
    excessYesOrNo: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceFrom: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceTo: Yup.string()
    .required('This field is Required!'),
    clientWorkAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientWorkAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    dateWhenLicenseWasObtained: Yup.string()
    .required('Date Is Required'),
    maritalStatus: Yup.string()
    .required('This Field Is Required')

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  nationalId: "",
  clientFirstName: "",
  clientLastName: "",
  clientEmailAddress: "",
  dateOfBirth: "",
  clientPhoneNumber: "",
  clientGender:"",
  clientAddressLineOne:"",
  clientAddressLineTwo:"",
  clientRegion:"",
  clientZip:"",
  clientCountry:"",
  vehicleRegistrationNumber:"",
  clientLicenseNumber:"",
  vehicleMake:"",
  vehicleModel:"",
  vehicleYear:"",
  vehicleMileage:"",
  vehicleWorth:"",
  vehicleUsage:"",
  numberOfSeats:"",
  alarmSystemYesOrNo:"",
  nightParkingYesOrNo:"",
  isVehicleUsedByOthers:"",
  dependentLicenseNumber:"",
  dependentNationalId: "",
  relationshipToBeneficiary: "",
  dependentFirstName: "",
  dependentLastName: "",
  dependentEmailAddress: "",
  dependentDateOfBirth: "",
  dependentPhoneNumber: "",
  dependentGender:"",
  carInsuranceType:"",
  thirdPartyLiability:"",
  allRisksYesOrNo:"",
  replacementCarYesOrNo:"",
  totalLossYesOrNo:"",
  excessYesOrNo:"",
  periodOfInsuranceFrom:"",
  periodOfInsuranceTo:"",

}

const onSubmit = (values, { setSubmitting, setErrors }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const VehicleDetails = () =>{


  

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        nationalId: true,
        clientFirstName: true,
        clientLastName: true,
        clientEmailAddress: true,
        dateOfBirth: true,
        clientPhoneNumber: true,
        clientGender: true,
        clientAddressLineOne:true,
        clientAddressLineTwo:true,
        clientRegion:true,
        clientZip:true,
        clientCountry:true,
        vehicleRegistrationNumber:true,
        clientLicenseNumber:true,
        vehicleMake:true,
        vehicleModel:true,
        vehicleYear:true,
        vehicleMileage:true,
        vehicleWorth:true,
        vehicleUsage:true,
        numberOfSeats:true,
        alarmSystemYesOrNo:true,
        nightParkingYesOrNo:true,
        isVehicleUsedByOthers:true,
        dependentLicenseNumber:true,
        dependentNationalId:true,
        relationshipToBeneficiary:true,
        dependentFirstName:true,
        dependentLastName:true,
        dependentEmailAddress:true,
        dependentDateOfBirth:true,
        dependentPhoneNumber:true,
        dependentGender:true,
        carInsuranceType:true,
        thirdPartyLiability:true,
        allRisksYesOrNo:true,
        replacementCarYesOrNo:true,
        totalLossYesOrNo:true,
        excessYesOrNo:true,
        periodOfInsuranceFrom:true,
        periodOfInsuranceTo:true,
      
      }
    )
    validateForm(errors)
  }
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Vehicle Details</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                status,
                dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                handleReset,
                setTouched
              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    
                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <Label for="licenseNumber">Client License Number</Label>
                        <Input type="text"
                              name="clientLicenseNumber"
                              id="clientLicenseNumber"
                              placeholder="License Number"
                              autoComplete="license number"
                              valid={!errors.clientLicenseNumber}
                              invalid={touched.clientLicenseNumber && !!errors.clientLicenseNumber}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientLicenseNumber} />
                        <FormFeedback>{errors.clientLicenseNumber}</FormFeedback>
                      </FormGroup>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <Label for="dateWhenLicenseWasObtained">When Did You Obtain Your License?</Label>
                        <Input type="date"
                              name="dateWhenLicenseWasObtained"
                              id="dateWhenLicenseWasObtained"
                              placeholder="yyyy/mm/dd"
                              autoComplete="dateWhenLicenseWasObtained"
                              valid={!errors.dateWhenLicenseWasObtained}
                              invalid={touched.dateWhenLicenseWasObtained && !!errors.dateWhenLicenseWasObtained}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.dateWhenLicenseWasObtained} />
                        <FormFeedback>{errors.dateWhenLicenseWasObtained}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="vehicleRegistrationNumber">Vehicle Registration Number</Label>
                            <Input type="text"
                                  name="vehicleRegistrationNumber"
                                  id="vehicleRegistrationNumber"
                                  placeholder="Vehicle Registration Number"
                                  autoComplete="vehicle registration number"
                                  valid={!errors.vehicleRegistrationNumber}
                                  invalid={touched.vehicleRegistrationNumber && !!errors.vehicleRegistrationNumber}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.vehicleRegistrationNumber} />
                            <FormFeedback>{errors.vehicleRegistrationNumber}</FormFeedback>
                          </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="vehicleMake">Vehicle Make</Label>
                        <Input type="select" name="vehicleMake" id="vehicleMake"
                        valid={!errors.vehicleMake}
                        invalid={touched.vehicleMake && !!errors.vehicleMake}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.vehicleMake} >
                          <option value="" disabled selected hidden>Select your Vehicle Make</option>   
                          <option value="ACURA">ACURA</option>
                          <option value="ASTON MARTIN">ASTON MARTIN</option>
                          <option value="AUDI">AUDI</option>
                          <option value="BENTLEY">BENTLEY</option>
                          <option value="BMW">BMW</option>
                          <option value="BUICK">BUICK</option>
                          <option value="CADILLAC">CADILLAC</option>
                          <option value="CHEVROLET">CHEVROLET</option>
                          <option value="CHRYSLER">CHRYSLER</option>
                          <option value="DODGE">DODGE</option>
                          <option value="FERRARI">FERRARI</option>
                          <option value="FORD">FORD</option>
                          <option value="GMC">GMC</option>
                          <option value="HONDA">HONDA</option>
                          <option value="HUMMER">HUMMER</option>
                          <option value="HYUNDAI">HYUNDAI</option>
                          <option value="INFINITI">INFINITI</option>
                          <option value="ISUZU">ISUZU</option>
                          <option value="JAGUAR">JAGUAR</option>
                          <option value="JEEP">JEEP</option>
                          <option value="KIA">KIA</option>
                          <option value="LAMBORGHINI">LAMBORGHINI</option>
                          <option value="LAND ROVER">LAND ROVER</option>
                          <option value="LEXUS">LEXUS</option>
                          <option value="LINCOLN">LINCOLN</option>
                          <option value="LOTUS">LOTUS</option>
                          <option value="MASERATI">MASERATI</option>
                          <option value="MAYBACH">MAYBACH</option>
                          <option value="MAZDA">MAZDA</option>
                          <option value="MERCEDES-BENZ">MERCEDES-BENZ</option>
                          <option value="MERCURY">MERCURY</option>
                          <option value="MINI">MINI</option>
                          <option value="MITSUBISHI">MITSUBISHI</option>
                          <option value="NISSAN">NISSAN</option>
                          <option value="PONTIAC">PONTIAC</option>
                          <option value="PORSCHE">PORSCHE</option>
                          <option value="ROLLS-ROYCE">ROLLS-ROYCE</option>
                          <option value="SAAB">SAAB</option>
                          <option value="SATURN">SATURN</option>
                          <option value="SUBARU">SUBARU</option>
                          <option value="SUZUKI">SUZUKI</option>
                          <option value="TOYOTA">TOYOTA</option>
                          <option value="VOLKSWAGEN">VOLKSWAGEN</option>
                          <option value="VOLVO">VOLVO</option>
                          <option value="other">Other</option>
                        </Input>
                        <FormFeedback>{errors.vehicleMake}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="vehicleModel">Vehicle Model</Label>
                      <Input type="text"
                            name="vehicleModel"
                            id="vehicleModel"
                            placeholder="Vehicle Model"
                            autoComplete="vehicle-model"
                            valid={!errors.vehicleModel}
                            invalid={touched.vehicleModel && !!errors.vehicleModel}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.vehicleModel} />
                      <FormFeedback>{errors.vehicleModel}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="vehicleYear">Vehicle Year</Label>
                            <Input type="date"
                                  name="vehicleYear"
                                  id="vehicleYear"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-vehicleYear"
                                  valid={!errors.vehicleYear}
                                  invalid={touched.vehicleYear && !!errors.vehicleYear}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.vehicleYear} />
                            <FormFeedback>{errors.vehicleYear}</FormFeedback>
                          </FormGroup>
                        </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <Label for="vehicleMileage">Vehicle Mileage</Label>
                          <Input type="text"
                                name="vehicleMileage"
                                id="vehicleMileage"
                                placeholder="Vehicle Mileage"
                                autoComplete="vehicleMileage"
                                valid={!errors.vehicleMileage}
                                invalid={touched.vehicleMileage && !!errors.vehicleMileage}
                                autoFocus={false}
                                required
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.vehicleMileage} />
                        <FormFeedback>{errors.vehicleMileage}</FormFeedback>

                      </FormGroup>
                      </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="vehicleWorth">Vehicle Worth</Label>
                        <Input type="select" name="vehicleWorth" id="vehicleWorth"
                        valid={!errors.vehicleWorth}
                        invalid={touched.vehicleWorth && !!errors.vehicleWorth}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.vehicleWorth} >
                          <option value="" disabled selected hidden>Select from the range</option>   
                          <option value = "1">0 - R100 000</option>
                          <option value = "2">R100 001 - R400 000</option>
                          <option value = "3">R400 000+</option>
                        </Input>
                        <FormFeedback>{errors.vehicleWorth}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <Label for="numberOfSeats">Number of Seats</Label>
                          <Input type="text"
                                name="numberOfSeats"
                                id="numberOfSeats"
                                placeholder="Number Of Seats"
                                autoComplete="numberOfSeats"
                                valid={!errors.numberOfSeats}
                                invalid={touched.numberOfSeats && !!errors.numberOfSeats}
                                autoFocus={false}
                                required
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.numberOfSeats} />
                        <FormFeedback>{errors.numberOfSeats}</FormFeedback>

                      </FormGroup>
                      </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="alarmSystemYesOrNo">Does your car have an alarm system?</Label>
                        <Input type="select" name="alarmSystemYesOrNo" id="alarmSystemYesOrNo"
                        valid={!errors.alarmSystemYesOrNo}
                        invalid={touched.alarmSystemYesOrNo && !!errors.alarmSystemYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.alarmSystemYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.alarmSystemYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="nightParkingYesOrNo">Do you have night parking at your place of residence?</Label>
                        <Input type="select" name="nightParkingYesOrNo" id="nightParkingYesOrNo"
                        valid={!errors.nightParkingYesOrNo}
                        invalid={touched.nightParkingYesOrNo && !!errors.nightParkingYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.nightParkingYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.nightParkingYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="isVehicleUsedByOthers">Is Vehicle Used by Others</Label>
                        <Input type="select" name="isVehicleUsedByOthers" id="isVehicleUsedByOthers"
                        valid={!errors.isVehicleUsedByOthers}
                        invalid={touched.isVehicleUsedByOthers && !!errors.isVehicleUsedByOthers}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.isVehicleUsedByOthers} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.isVehicleUsedByOthers}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>
                   
                    </Form>

                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default VehicleDetails;
