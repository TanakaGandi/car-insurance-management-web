import React from "react";
import { Row, Col } from "reactstrap";
import {Link} from "react-router-dom"
import ScreensCards from "../../components/cards/ScreensCards";

const NewApplication = () => {
  return (
    <>
     <Link to="/dashboard" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"New Application"}
            footerText={"Apply"}
            cardRoute="new/type"/>
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Underwriter Tasks"}
            footerText={"View Tasks"}
            cardRoute="task/underwriter"/>
        </Col>
      </Row>
    </>
  );
};

export default NewApplication;
