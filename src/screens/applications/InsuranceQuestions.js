import React from 'react';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';
import StepWizard from "react-step-wizard";

import * as Yup from 'yup'


const validationSchema = function (values) {
  return Yup.object().shape({
    nationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    clientFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    clientLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    clientEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    clientPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    clientGender: Yup.string()
    .required('Gender is required!'),
    clientAddressLineOne: Yup.string()
    .required('Address Line One Required!'),
    clientAddressLineTwo: Yup.string()
    .required('Address Line Two Required!'),
    clientRegion: Yup.string()
    .required('Region is Required!'),
    clientZip: Yup.string()
    .required('Zip is Required!'),
    clientCountry: Yup.string()
    .required('Client Country is Required!'),
    vehicleRegistrationNumber: Yup.string()
    .required('Vehicle Registration Number is Required!'),
    clientLicenseNumber: Yup.string()
    .required('Client License Number is Required!'),
    vehicleMake: Yup.string()
    .required('Vehicle Make is Required!'),
    vehicleModel: Yup.string()
    .required('Vehicle Model is Required!'),
    vehicleYear: Yup.string()
    .required('Vehicle Year is Required!'),
    vehicleMileage: Yup.string()
    .required('Vehicle Mileage is Required!'),
    vehicleWorth: Yup.string()
    .required('Vehicle Worth is Required!'),
    vehicleUsage: Yup.string()
    .required('Vehicle Usage is Required!'),
    numberOfSeats: Yup.string()
    .required('This field is Required!'),
    alarmSystemYesOrNo: Yup.string()
    .required('This field is Required!'),
    nightParkingYesOrNo: Yup.string()
    .required('This field is Required!'),
    isVehicleUsedByOthers: Yup.string()
    .required('This field is Required!'),
    dependentNationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),
    dependentLicenseNumber: Yup.string()
    .required('License Number is Required!'),
    relationshipToBeneficiary: Yup.string()
    .required('Relationship to Beneficiary is Required!'),
    dependentFirstName: Yup.string()
    .min(2, `First Name has to be at least 2 characters`)
    .required('First Name is required'),
    dependentLastName: Yup.string()
    .min(2, `Last Name has to be at least 2 characters `)
    .required('Last Name is required'),
    dependentEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dependentDateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    dependentPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    dependentGender: Yup.string()
    .required('Gender is required!'),
    carInsuranceType: Yup.string()
    .required('Type of Car Insurance is Required!'),
    thirdPartyLiability: Yup.string()
    .required('Third Party Liability is Required!'),
    allRisksYesOrNo: Yup.string()
    .required('This field is Required!'),
    replacementCarYesOrNo: Yup.string()
    .required('This field is Required!'),
    totalLossYesOrNo: Yup.string()
    .required('This field is Required!'),
    excessYesOrNo: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceFrom: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceTo: Yup.string()
    .required('This field is Required!'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  nationalId: "",
  clientFirstName: "",
  clientLastName: "",
  clientEmailAddress: "",
  dateOfBirth: "",
  clientPhoneNumber: "",
  clientGender:"",
  clientAddressLineOne:"",
  clientAddressLineTwo:"",
  clientRegion:"",
  clientZip:"",
  clientCountry:"",
  vehicleRegistrationNumber:"",
  clientLicenseNumber:"",
  vehicleMake:"",
  vehicleModel:"",
  vehicleYear:"",
  vehicleMileage:"",
  vehicleWorth:"",
  vehicleUsage:"",
  numberOfSeats:"",
  alarmSystemYesOrNo:"",
  nightParkingYesOrNo:"",
  isVehicleUsedByOthers:"",
  dependentLicenseNumber:"",
  dependentNationalId: "",
  relationshipToBeneficiary: "",
  dependentFirstName: "",
  dependentLastName: "",
  dependentEmailAddress: "",
  dependentDateOfBirth: "",
  dependentPhoneNumber: "",
  dependentGender:"",
  carInsuranceType:"",
  thirdPartyLiability:"",
  allRisksYesOrNo:"",
  replacementCarYesOrNo:"",
  totalLossYesOrNo:"",
  excessYesOrNo:"",
  periodOfInsuranceFrom:"",
  periodOfInsuranceTo:"",

}

const onSubmit = (values, { setSubmitting, setErrors }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const InsuranceQuestions = () =>{


  

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        nationalId: true,
        clientFirstName: true,
        clientLastName: true,
        clientEmailAddress: true,
        dateOfBirth: true,
        clientPhoneNumber: true,
        clientGender: true,
        clientAddressLineOne:true,
        clientAddressLineTwo:true,
        clientRegion:true,
        clientZip:true,
        clientCountry:true,
        vehicleRegistrationNumber:true,
        clientLicenseNumber:true,
        vehicleMake:true,
        vehicleModel:true,
        vehicleYear:true,
        vehicleMileage:true,
        vehicleWorth:true,
        vehicleUsage:true,
        numberOfSeats:true,
        alarmSystemYesOrNo:true,
        nightParkingYesOrNo:true,
        isVehicleUsedByOthers:true,
        dependentLicenseNumber:true,
        dependentNationalId:true,
        relationshipToBeneficiary:true,
        dependentFirstName:true,
        dependentLastName:true,
        dependentEmailAddress:true,
        dependentDateOfBirth:true,
        dependentPhoneNumber:true,
        dependentGender:true,
        carInsuranceType:true,
        thirdPartyLiability:true,
        allRisksYesOrNo:true,
        replacementCarYesOrNo:true,
        totalLossYesOrNo:true,
        excessYesOrNo:true,
        periodOfInsuranceFrom:true,
        periodOfInsuranceTo:true,
      
      }
    )
    validateForm(errors)
  }
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Insurance Questions</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                status,
                dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                handleReset,
                setTouched
              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    
                
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="carInsuranceType">Type of Car Insurance</Label>
                        <Input type="select" name="carInsuranceType" id="carInsuranceType"
                        valid={!errors.carInsuranceType}
                        invalid={touched.carInsuranceType && !!errors.carInsuranceType}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.carInsuranceType} >
                          <option value="" disabled selected hidden>Select Coverage</option> 
                          <option value="liabilityCoverage">Liability Coverage</option>
                          <option value="collisionCoverage">Collision Coverage</option>
                          <option value="comprehensiveCoverage">Comprehensive Coverage</option>
                          <option value="personalInjuryProtection">Personal Injury Protection</option>
                        </Input>
                        <FormFeedback>{errors.carInsuranceType}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="thirdPartyLiability">Third Party Insurance</Label>
                        <Input type="select" name="thirdPartyLiability" id="thirdPartyLiability"
                        valid={!errors.thirdPartyLiability}
                        invalid={touched.thirdPartyLiability && !!errors.thirdPartyLiability}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.thirdPartyLiability} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                        </Input>
                        <FormFeedback>{errors.thirdPartyLiability}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="allRisksYesOrNo">All Risks</Label>
                        <Input type="select" name="allRisksYesOrNo" id="allRisksYesOrNo"
                        valid={!errors.allRisksYesOrNo}
                        invalid={touched.allRisksYesOrNo && !!errors.allRisksYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.allRisksYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.allRisksYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="replacementCarYesOrNo">Would You Want A Replacement Car?</Label>
                        <Input type="select" name="replacementCarYesOrNo" id="replacementCarYesOrNo"
                        valid={!errors.replacementCarYesOrNo}
                        invalid={touched.replacementCarYesOrNo && !!errors.replacementCarYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.replacementCarYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.replacementCarYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="totalLossYesOrNo">Total Loss</Label>
                        <Input type="select" name="totalLossYesOrNo" id="totalLossYesOrNo"
                        valid={!errors.totalLossYesOrNo}
                        invalid={touched.totalLossYesOrNo && !!errors.totalLossYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.totalLossYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.totalLossYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="excessYesOrNo">Excess</Label>
                        <Input type="select" name="excessYesOrNo" id="excessYesOrNo"
                        valid={!errors.excessYesOrNo}
                        invalid={touched.excessYesOrNo && !!errors.excessYesOrNo}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.excessYesOrNo} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.excessYesOrNo}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <strong>Period of Insurance</strong>
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="periodOfInsuranceFrom">From:</Label>
                            <Input type="date"
                                  name="periodOfInsuranceFrom"
                                  id="periodOfInsuranceFrom"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-periodOfInsuranceFrom"
                                  valid={!errors.periodOfInsuranceFrom}
                                  invalid={touched.periodOfInsuranceFrom && !!errors.periodOfInsuranceFrom}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.periodOfInsuranceFrom} />
                            <FormFeedback>{errors.periodOfInsuranceFrom}</FormFeedback>
                          </FormGroup>
                        </Col>
                      
                        <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                          <Label for="periodOfInsuranceTo">To:</Label>
                          <Input type="date"
                                name="periodOfInsuranceTo"
                                id="periodOfInsuranceTo"
                                placeholder="yyyy/mm/dd"
                                autoComplete="new-periodOfInsuranceTo"
                                valid={!errors.periodOfInsuranceTo}
                                invalid={touched.periodOfInsuranceTo && !!errors.periodOfInsuranceTo}
                                required
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.periodOfInsuranceTo} />
                          <FormFeedback>{errors.periodOfInsuranceTo}</FormFeedback>
                        </FormGroup>
                      </Col>
                    </FormGroup>

                    
                    </Form>

                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default InsuranceQuestions;
