import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const onSuccess = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Claim Submitted!", "Thank you!", "success");
};

export const onError = () => {
  const MySwal = withReactContent(Swal);
  MySwal.fire("Error Submitting Claim!", "Please Try Again!", "error");
};
